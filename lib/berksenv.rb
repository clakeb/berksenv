require 'thor'
require 'fileutils'
require "berksenv/version"
require 'pathname'

module BerksEnv
  class Tasks < Thor

    desc "list", "list the available berks environments"
    def list
      current = current_environment
      available_environments.each do |env|
        puts "#{(env == current) ? '*' : ' '} #{env}"
      end
      if available_environments.empty?
        puts "No Environments Available"
      end
    end

    desc "use ENV", "use a particular environment"
    def use(env="")
      if available_environments.include?(env)
        select_environment(env)
      else
        puts "'#{env}' is not a known environment."
      end
      list
    end

    desc "current", "display the name of the current environment"
    def current()
      puts current_environment
    end

    desc "init", "initialize the environment"
    def init()
      FileUtils.mkdir_p(berksenv_dir) unless File.directory?(berksenv_dir)
      FileUtils.mkdir_p(environments_dir) unless File.directory?(environments_dir)
      FileUtils.safe_unlink(berks_dir)
      list
    end

    no_tasks do

      def berks_dir
        File.expand_path("~/.berkshelf")
      end

      def berksenv_dir
        dir = ENV['BERKSENV_DIR'].to_s
        dir = File.expand_path("~/.berksenv") if dir.empty?
        dir
      end

      def environments_dir
        File.expand_path('environments', berksenv_dir)
      end

      def environment_dir(environment)
        File.expand_path(environment, environments_dir)
      end

      def available_environments
        Pathname.new(environments_dir).children.select { |c| c.directory? }.map { |d| d.basename.to_s }
      end

      def environment_available?(env)
        available_environments.include?(env)
      end

      def select_environment(env)
        unless current_environment == env
          File.write(File.expand_path("#{berksenv_dir}/current"), env)
          FileUtils.safe_unlink(berks_dir)
          FileUtils.symlink(File.join(environments_dir, env), berks_dir)
        end
      end

      def current_environment
        begin
          Pathname.new(File.readlink(File.expand_path('~/.berkshelf'))).split.last.to_s
        rescue Errno::ENOENT
          nil
        end
      end
    end

  end
end
