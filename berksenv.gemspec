# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'berksenv/version'

Gem::Specification.new do |gem|
  gem.name          = "berksenv"
  gem.version       = Berksenv::VERSION
  gem.authors       = ["C. Blake Barber"]
  gem.email         = ["clakeb@gmail.com"]
  gem.description   = %q{Manage your current Berks server endpoint}
  gem.summary       = gem.description
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_dependency "thor"
end
